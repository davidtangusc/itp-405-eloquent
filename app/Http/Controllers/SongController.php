<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Song;
use App\Artist;

class SongController extends Controller
{
    public function edit($songID)
    {
        $artists = DB::table('artists')
            ->orderBy('artist_name')
            ->get();
        // $artists = Artist::orderBy('artist_name')->get();

        $song = DB::table('songs')
            ->where('id', '=', $songID)
            ->first();
        // $song = Song::find($songID);
        // dd($song);

        return view('songs.edit', [
            'artists' => $artists,
            'song' => $song
        ]);
    }

    public function destroy($songID)
    {
        $song = Song::find($songID);
        $song->delete();
        // DB::table('songs')
        //     ->where('id', '=', $songID)
        //     ->delete();

        return redirect('/songs')
            ->with('successStatus', 'Song was deleted successfully!');
    }

    public function create()
    {
        $artists = DB::table('artists')
            ->orderBy('artist_name')
            ->get();
        return view('songs.create', [
            'artists' => $artists
        ]);
    }

    public function update($songID)
    {
        $validation = Validator::make([
            'title' => request('title'),
            'price' => request('price'),
            'artist' => request('artist')
        ], [
            'title' => 'required',
            'price' => 'required|numeric',
            'artist' => 'required'
        ]);

        if ($validation->passes()) {
            DB::table('songs')
                ->where('id', '=', $songID)
                ->update([
                    'title' => request('title'),
                    'price' => request('price'),
                    'artist_id' => request('artist')
                ]);

            return redirect('/songs')
                ->with('successStatus', 'Song was updated successfully!');
        } else {
            return redirect("/songs/$songID/edit")
                ->withErrors($validation);
        }
    }

    public function store(Request $request)
    {
        $validation = Validator::make($request->all() /*[
            'title' => request('title'),
            'price' => request('price'),
            'artist' => request('artist')
        ]*/, [
            'title' => 'required',
            'price' => 'required|numeric',
            'artist' => 'required'
        ]);

        if ($validation->passes()) {
            DB::table('songs')->insert([
                'title' => request('title'),
                'price' => request('price'),
                'artist_id' => request('artist')
            ]);

            return redirect('/songs')
                ->with('successStatus', 'Song was created successfully!');
        } else {
            return redirect('/songs/new')
                ->withInput()
                ->withErrors($validation);
        }


    }

    public function index()
    {
        $songs = DB::table('songs')
            ->select('songs.id', 'artist_name', 'title', 'price')
            ->join('artists', 'songs.artist_id', '=', 'artists.id')
            ->orderBy('artist_name')
            ->get();

        // resources/views/songs/index.blade.php
        return view('songs.index', [
            'songs' => $songs
        ]);
    }
}
