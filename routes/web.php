<?php

use App\Genre;
use App\Song;
use App\Artist;
use App\Product;

Route::get('/orm-relationships', function() {
    // $songs = Song::all(); // N + 1 problem
    $songs = Song::with('artist', 'genre')->get();
    // var_dump(count($songs));

    foreach ($songs as $song) {
        var_dump($song->id);
        if ($song->genre) {
            var_dump($song->genre->genre);
        } else {
            echo 'no genre for this song';
        }

        echo '<hr>';
        // var_dump($song->artist->artist_name);
    }

    // $song = Song::find(130);
    // return $song->genre;

    // $artist = Artist::find(3);
    // $songs = $artist->songs;
    // return $songs;
});

// DB::listen(function($query) {
//     echo $query->sql;
//     echo '<hr>';
// });

Route::get('/orm-crud', function() {
  $product = Product::find(2);
  $product->delete();
  return $product;
  // $product = new Product();
  // $product->name = 'davids shoe';
  // $product->description = 'black and white';
  // $product->price = 60;
  // $product->save(); // insert
  // $product->price = 100;
  // $product->save(); // update
  // return $product;
  // return Song::find(3);
  // $genres = Genre::all();
  // return $genres;
  // $songs = Song::all();
  // return $songs;
});













Route::get('/', function () {
    return view('welcome');
});

Route::get('/songs', 'SongController@index');
Route::get('/songs/new', 'SongController@create'); // show create song form
Route::post('/songs', 'SongController@store'); // handle create song form submit
Route::get('/songs/{id}/delete', 'SongController@destroy');
Route::get('/songs/{id}/edit', 'SongController@edit');
Route::post('/songs/{id}', 'SongController@update');
