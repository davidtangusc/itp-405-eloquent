<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Songs</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      @if (session('successStatus'))
        <div class="alert alert-success" role="alert">
          {{ session('successStatus') }}
        </div>
      @endif
      <a href="/songs/new" class="btn">Create a Song</a>
      <table class="table">
        <thead>
          <tr>
            <th>Song Title</th>
            <th>Artist</th>
            <th colspan="2">Price</th>
          </tr>
        </thead>
        <tbody>
          @foreach ($songs as $song)
            <tr>
              <td>{{ $song->title }}</td>
              <td>{{ $song->artist_name }}</td>
              <td>{{ $song->price }}</td>
              <td>
                <a href="/songs/{{ $song->id }}/delete" class="btn">Delete</a>
                <span>|</span>
                <a href="/songs/{{ $song->id }}/edit">Edit</a>
              </td>
            </tr>
          @endforeach
        </tbody>
      </table>
    </div>

  </body>
</html>
