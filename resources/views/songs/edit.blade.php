<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Edit Song</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  </head>
  <body>
    <div class="container">
      @if (count($errors) > 0)
        <div class="alert alert-danger">
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
      @endif
      <h1>Edit Song</h1>
      <form action="/songs/{{ $song->id }}" method="post">
        {{ csrf_field() }}
        <div class="form-group">
          <label for="title">Title</label>
          <input type="text" name="title" id="title" class="form-control" value="{{ $song->title }}">
        </div>
        <div class="form-group">
          <label for="price">Price</label>
          <input type="text" name="price" id="price" class="form-control" value="{{ $song->price }}">
        </div>
        <div class="form-group">
          <label for="artist">Artist</label>
          <select name="artist" id="artist" class="form-control">
            @foreach ($artists as $artist)
              @if ($artist->id === $song->artist_id)
                <option value="{{ $artist->id }}" selected>
              @else
                <option value="{{ $artist->id }}">
              @endif
                  {{ $artist->artist_name }}
                </option>
            @endforeach
          </select>
        </div>
        <button type="submit" class="btn btn-primary">Save</button>
      </form>
    </div>


  </body>
</html>
